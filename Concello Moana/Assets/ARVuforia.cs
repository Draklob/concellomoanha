﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARVuforia : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKeyDown( KeyCode.Escape ) )
        {
            OnBackPressed(  );
        }
    }

    private void OnBackPressed( )
    {
        SceneManager.LoadScene( "MainApp" );
        MainMenu.Show(  );
        
    }
}
