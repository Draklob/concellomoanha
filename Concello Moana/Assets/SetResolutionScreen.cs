﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SetResolutionScreen : MonoBehaviour
{
    private Vector2 lastScreenSize;
    void Awake()
    {
        lastScreenSize = new Vector2( Screen.width, Screen.height);
    }
    void Update()
    {
        Vector2 screenSize = new Vector2( Screen.width, Screen.height);
        if ( screenSize != lastScreenSize )
        {
            Debug.Log( "Cambiamos screen" );
            CanvasScaler canvasScaler = GetComponent<CanvasScaler>();
            canvasScaler.referenceResolution = lastScreenSize = new Vector2(Screen.width, Screen.height);            
        }
        
    }
}
