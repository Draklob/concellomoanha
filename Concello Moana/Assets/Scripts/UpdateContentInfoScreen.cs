﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Calcula y actualiza la pantalla de informacion para algunas pantallas con varias imagenes y textos.
/// </summary>
public class UpdateContentInfoScreen : MonoBehaviour
{
	// Array de textos para saber sus medidas y añadirlos al height content global 
    public RectTransform[] _textos;
	// Array de imagenes para saber sus medidas y añadirlos al height content global 
	public RectTransform[ ] _imagenes;
    
	// Transform del objeto que tiene el contenido total a mostrar
    private	RectTransform rt;
    
	// Use this for initialization
    void Start ()
    {
    	rt = GetComponent< RectTransform >( );
    	Invoke(nameof( SetContentSize ), 0.01f); // Invoke method on your start function to delay it a bit.   
    }
    
    private void SetContentSize( )
    {
	    //rt.sizeDelta= new Vector2( 0f, _transform.rect.height);
	    float sizeTotalTexts = 0;
	    float sizeTotalImages = 0;

	    for ( int i = 0; i < _textos.Length; i++ )
	    {
		    sizeTotalTexts += _textos[ i ].rect.height;
	    }
	    
	    for ( int i = 0; i < _imagenes.Length; i++ )
	    {
		    sizeTotalImages += _imagenes[ i ].rect.height;
	    }
	    
	    Debug.Log( sizeTotalTexts);
	    rt.sizeDelta = new Vector2( rt.rect.width, 1500 + 100 * (_textos.Length + _imagenes.Length)  + sizeTotalTexts + sizeTotalImages);
    }
	
	// En el futuro podemos valorar colocar cada texto e imagen por codigo, teniendo en  cuenta la posicion Y negativa
	// para cada elemento -700, -1550, etc, teniendo en cuenta las medidas de cada elemento.
	private void SetElementsUI()
	{}
}
