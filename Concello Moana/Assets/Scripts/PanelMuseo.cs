﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PanelMuseo : MonoBehaviour
{

	private bool active;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetTouch( 0 ).phase == TouchPhase.Began)
		{
			Debug.Log( "CLIKING" );
			if ( EventSystem.current.IsPointerOverGameObject( ) )
				return;

			if ( active )
				PanelOnOff( );
		}
	}

	public void PanelOnOff( )
	{
		active = !active;
		gameObject.SetActive( active );
	}
}
