﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Preloader : MonoBehaviour
{
	public bool enableSplashScreen;
	public GameObject splashScreenPrefab;
	public GameObject menuManager;
	
	private Animator _animator;
	private bool appAlreadyOn;
	
	private void Awake( )
	{
		
	}

	private void Start( )
	{
		Debug.Log( "INIciando" );
		if ( !enableSplashScreen || appAlreadyOn )
		{
			InitApp(  );
			//Destroy( splashScreenPrefab.gameObject );
		}
		
		if ( enableSplashScreen )
		{
			splashScreenPrefab = Instantiate( splashScreenPrefab );

			_animator = splashScreenPrefab.GetComponent< Animator >( );
			_animator.SetBool( "EnableSplashScreen", enableSplashScreen );	
		}
	}

	public void InitApp( )
	{
		if ( !appAlreadyOn )
		{
			Instantiate( menuManager );
			ChooseLanguageScreen.Show( );
			appAlreadyOn = true;
		}
		else
		{
			//MainMenu.Show(  );
		}
	}

	public void DestroySplashScreen( )
	{
		DestroyImmediate( splashScreenPrefab, true );
	}
	
	/*
	// Test to do Splash Screen by Code
	
	//public GameObject[ ] itemsToLoad;

	private CanvasGroup fadeGroup;
	private float loadTime;
	private float minimumLogoTime = 2.0f; // Minimum time of that scene
	private float timer;
	
	void Start () {
		// Grab the only canvasgroup in the scene
		fadeGroup = FindObjectOfType< CanvasGroup >( );
		
		// Start with a whith screen
		fadeGroup.alpha = 1f;
		
		// Preload the game, if we need to load stuff for the app, we should do it here.
		// $$
		
		// Get a tiumestamp of the completion time
		// if loadtime is super, give it a small buffer time so we can apreciate the logo
		if ( Time.time < minimumLogoTime )
			loadTime = minimumLogoTime;
		else
		{
			loadTime = Time.time;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
//		timer += Time.deltaTime;
//		Debug.Log( itemsToLoad.Length );
//
//		for ( int i = 0; i < itemsToLoad.Length; i++ )
//		{
//			itemsToLoad[i].SetActive( true );
//			// Fade-in
//			if ( timer < minimumLogoTime )
//			{
//				fadeGroup.alpha = 1 - timer;
//			}
//			// Fade-out
//			if ( timer > minimumLogoTime && loadTime != 0 )
//			{
//				fadeGroup.alpha = timer - minimumLogoTime;
//				if ( fadeGroup.alpha >= 1)
//					itemsToLoad[i].SetActive( false );
//				else if ( fadeGroup.alpha >= 1 && i == itemsToLoad.Length)
//					ChooseLanguageScreen.Show( );
//
//			}
//		}
//		
	}
	*/
}
