﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Historia ou Lenda Info", menuName = "App/HistoriaLenda")]
public class HistoriaELendas : ScriptableObject
{
    [HideInInspector] public int ID;
    
    public Sprite background;
    public string titleText;
    public string[ ] subTexts;
    public Sprite[ ] subImages;
    
}
