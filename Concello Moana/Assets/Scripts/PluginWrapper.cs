﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PluginWrapper : MonoBehaviour
{
	public Text txt;
	private AndroidJavaObject javaClass;
	// Use this for initialization
	void Start () {
		javaClass = new AndroidJavaObject( "com.draklob.testlibrary.TutorialTest");
		//txt.text = javaClass.CallStatic< string >( "GetTextFromPlugin", 7 );
		//javaClass.Call( "ShowingMessageFromPlugin" );
		//javaClass.Call( "LogNumberSentFromUnity", 6 );
		//txt.text = javaClass.Call< int >( "AddFiveToMyNumber", 10 ).ToString();
		
		//javaClass.Call( "AlturaUI" );
		//javaClass.Call( "HeightUI" );
		//javaClass.Call( "StatusBar" );
	}

	public void CallJavaFunction( String value )
	{
		javaClass.Call( "CallAorB", value );
	}
	
	public void ChangeTextToA( string value)
	{
		txt.text = "A" + value;
	}
	
	public void ChangeTextToB( string value)
	{
		txt.text = "B" + value;
	}
}
