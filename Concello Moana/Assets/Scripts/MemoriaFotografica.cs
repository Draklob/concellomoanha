﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Memorial Photo", menuName = "App/FotoMemorial")]
public class MemoriaFotografica : ScriptableObject
{
    // En principio no necesitamos ningun identificador, en el futuro ya se valorara o si necesitamos un diccionario.
   [HideInInspector]
    public int ID;

    //INFORMATION GALERIA MEMORIA FOTOGRAFICA
    public Sprite portraitGallery;
    public string title;
    public string autor;
    //public string coordinates;

	
    // INFORMATION MEMORIA FOTOGRAFICA FOTO
    public Sprite portraitPhoto;
    //public string titlePhoto;
    public bool landscapePhoto;
}
