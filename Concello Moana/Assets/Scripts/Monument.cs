﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Monument", menuName = "App/Monumento")]
public class Monument : ScriptableObject
{
	// En principio no necesitamos ningun identificador, en el futuro ya se valorara o si necesitamos un diccionario.
	[HideInInspector]
	public int ID;

	public string title;
	public string description;

	public Sprite portrait;
	
	// INFORMATION ABOUT THE MONUMENT
	public Sprite portraitInfo;
	public string textInfo;

	// Museo3D Screen
	public GameObject monument3D;
	public float initialZoom;
}
