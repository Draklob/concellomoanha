﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MemoriaFotograficaDisplay : MonoBehaviour
{
    public MemoriaFotografica memoriaFotografica;

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI autor;

    public Image portraitImage;


    private void Start( )
    {
        titleText.SetText( memoriaFotografica.title );
        autor.SetText( memoriaFotografica.autor );

        portraitImage.sprite = memoriaFotografica.portraitGallery;
    }

    public void WatchMemorialPictureInfo( )
    {
        //InfoMonumentScreen.Show( monument );
    }
}
