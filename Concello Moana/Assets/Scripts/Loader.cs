﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{

	public GameObject menuManager;
	public GameObject localizationManager;

	private bool isReady = false;
	
	// Use this for initialization
	void Start ()
	{
		//Instantiate( menuManager );
		Instantiate( localizationManager );
	}
	
}
