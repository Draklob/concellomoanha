﻿using TMPro;
using UnityEngine;

public class LocalizedText : MonoBehaviour
{
	public string key;
	
	// Use this for initialization
	void Start ()
	{
		TextMeshProUGUI text = GetComponent< TextMeshProUGUI >( );
		text.text = LocalizationManager.instance.GetLocalizedValue( key );
	}
}
