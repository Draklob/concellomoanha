﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

public class LocalizedTextEditor : EditorWindow
{
	public LocalizationData localizationData;

	[MenuItem("Window/Localized Text Editor")]
	static void Init( )
	{
		GetWindow( typeof( LocalizedTextEditor) ).Show();
	}

	private void OnGUI( )
	{
		if ( localizationData != null )
		{
			SerializedObject serializedObject = new SerializedObject( this );
			SerializedProperty serializedProperty = serializedObject.FindProperty( "localizationData" );
			EditorGUILayout.PropertyField( serializedProperty, true );
			serializedObject.ApplyModifiedProperties( );

			if ( GUILayout.Button( "Save data" ) )
			{
				SaveGameData(  );
			}
		}

		if ( GUILayout.Button( "Load data" ) )
		{
			LoadGameData(  );
		}

		if ( GUILayout.Button( "Create new data" ) )
		{
			CreateNewData(  );
		}
	}

	private void LoadGameData( )
	{
		string filePath =
			EditorUtility.OpenFilePanel( "Select localzation data file", Application.streamingAssetsPath, "json" );

		if ( !string.IsNullOrEmpty( filePath ) )
		{
			string dataAsJson = File.ReadAllText( filePath );
			localizationData = JsonUtility.FromJson< LocalizationData >( dataAsJson );
		}
	}
	
	private void SaveGameData( )
	{
		string filepath = EditorUtility.SaveFilePanel( "Save localization data file", Application.streamingAssetsPath,
			"", "json" );

		if ( !string.IsNullOrEmpty( filepath ) )
		{
			string dataAsJson = JsonUtility.ToJson( localizationData );
			File.WriteAllText( filepath, dataAsJson );
			
		}
	}
	
	private void CreateNewData( )
	{
		localizationData = new LocalizationData(  );	
	}
}
