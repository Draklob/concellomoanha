﻿using System;
using UnityEngine;
using UnityEngine.UI;

public abstract class Menu<T> : Menu where T : Menu<T>
{
	public static T Instance { get; private set; }

	// register our instance
	protected virtual void Awake()
	{
		Instance = (T)this;
		
		Canvas _canvas = GetComponent< Canvas >( );
		if ( _canvas != null )
		{
			_canvas.worldCamera = GameObject.FindWithTag( "MainCamera" ).GetComponent<Camera>(  );
//			if ( _canvas.renderMode == RenderMode.ScreenSpaceCamera )
//			{
//				CanvasScaler canvasScaler = GetComponent<CanvasScaler>(  );
//				canvasScaler.referenceResolution =  new Vector2( Screen.width, Screen.height);
//			}
		}
	}

	// unregister our instance
	protected virtual void OnDestroy()
	{
		Instance = null;
	}

	protected static void Open()
	{
		if (Instance == null) // Menu is already open
		{
			MenuManager.Instance.CreateInstance<T>();
		}
		else
		{
			Instance.gameObject.SetActive( true );
		}
		
		MenuManager.Instance.OpenMenu( Instance );
	}

	protected static void Close()
	{
		if (Instance == null)
		{
			Debug.LogErrorFormat("Trying to close menu {0} but Instance is null", typeof(T));
			return;
		}
		
		MenuManager.Instance.CloseMenu( Instance );
	}

	// If we only close the current menu, no need to declare this in the other inherited classes.
	public override void OnBackPressed()
	{
		Close();
	}

}

public abstract class Menu : MonoBehaviour
{
	[Tooltip("Destroy the Game Object when menu is closed (reduces memory usage)")]
	public bool DestroyWhenClosed = true;

	[Tooltip("Disable menus that are under this one in the stack")]
	public bool DisableMenusUnderneath = true;
	
	public abstract void OnBackPressed();
}