﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

// CLASS TO MANAGE THE CREATION OF MENUS
public class MenuManager : MonoBehaviour
{
	public ChooseLanguageScreen chooseLanguagePrefab;
	public MainMenu mainMenuPrefab;
	public HistoriaScreen historiaScreenPrefab;
	public PatrimonioCulturaScreen patrimonioScreenPrefab;
	public HistoriaGeneralScreen historiaGeneralScreenPrefab;
	public TurismoScreen turismoScreenPrefab;
	public GalleryScreen galleryScreenPrefab;
	public MuseumScreen museumScreenPrefab;
	public InfoMonumentScreen infoMonumentScreenPrefab;
	public InfoHistoriaScreen infoHistoriaScreenPrefab;
	public ConcelloScreen concelloScreenPrefab;
	public RutasScreen rutasScreenPrefab;
	public PraiasScreen praiasScreenPrefab;
	public InfoPraiaScreen InfoPraiaScreenPrefab;
	public MemoryPhotoGallery_Screen memoryPhotoGalleryScreenPrefab;
	public PhotoMemoryScreen photoMemoryScreenPrefab;
	public InfoRutaScreen infoRutaScreenPrefab;
	public GastronomiaScreen gastronomiaScreenPrefab;
	
	public static MenuManager Instance { get; private set; }
	
	private Stack<Menu> menuStack = new Stack<Menu>();

	private void Awake()
	{
		// Desactivamos la pantalla completa en los dispositivos.
	//	Screen.fullScreen = false;

			if ( Instance == null )
			{
				Instance = this;
			}
			else if( Instance != null )
			{
				Destroy( gameObject );
			}
			DontDestroyOnLoad( gameObject );

		Instance = this;

		//ChooseLanguageScreen.Show( );
	}

	private void OnDestroy()
	{
		Instance = null;
	}

	public void CreateInstance<T>() where T : Menu
	{
		var prefab = GetPrefab<T>();
	
		Instantiate(prefab, transform);
	}
	
	private T GetPrefab<T>() where T : Menu
	{
		// Get prefab dynamically, based on public fields set from Unity
		// You can use private fields with SerializeField attribute too
		var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly); // ??????
		foreach (var field in fields)
		{
			var prefab = field.GetValue(this) as T;
			if (prefab != null)
			{
				return prefab;
			}
		}
		// ??? What is this MissingReferenceException ???
		throw new MissingReferenceException("Prefab not found for type " + typeof(T));
	}


	public void OpenMenu(Menu instance)
	{
		//Debug.Log( instance );
		// De-activate top menu
		if (menuStack.Count > 0)
		{
			if (instance.DisableMenusUnderneath)
			{
				foreach (var menu in menuStack)
				{
//					if ( menuStack.Count == 1 )
//						continue;
					menu.gameObject.SetActive( false );
					
					if (menu.DisableMenusUnderneath)
					{
						break;
					}
				}
			}

			var topCanvas = instance.GetComponent<Canvas>();
			var previousCanvas = menuStack.Peek().GetComponent<Canvas>();
			if( topCanvas != null )
				topCanvas.sortingOrder = previousCanvas.sortingOrder + 1;
		}
		
		menuStack.Push( instance );
	}

	public void CloseMenu( Menu menu)
	{
		Debug.Log( "Closing Menu" );
		if (menuStack.Count == 0)
		{
			Debug.LogErrorFormat(menu, "{0} cannot be closed because menu stack is empty", menu.GetType());
			return;
		}

		if (menuStack.Peek() != menu)
		{
			Debug.LogErrorFormat(menu, "{0} cannot be closed because it is not on top of stack", menu.GetType());
			return;
		}

		CloseTopMenu();
	}

	private void CloseTopMenu()
	{
		var instance = menuStack.Pop();

		if (instance.DestroyWhenClosed)
		{
			Destroy( instance.gameObject );
		}
		else
		{
			instance.gameObject.SetActive( false );
		}
		
		// Re-activate top menu
		// If a re-activated menu is an overlay we need to activate the menu under it
		foreach (var menu in menuStack)
		{
			menu.gameObject.SetActive(true);

			if (menu.DisableMenusUnderneath)
				break;
		}
		
	}
	
	private void Update()
	{
		// On Android the back button is sent as Esc
		if (Input.GetKeyDown(KeyCode.Escape) && menuStack.Count > 0)
		{
			//Debug.Log( menuStack.Peek().gameObject.name);
			BackButton(  );
		}
	}

	public void BackButton( )
	{
		Instance.menuStack.Peek().OnBackPressed();
	}

}
