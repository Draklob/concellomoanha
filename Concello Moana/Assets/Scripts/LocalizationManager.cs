﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
	public static LocalizationManager instance;
	
	private Dictionary< string, string > localizedText;
	private bool isReady;
	private string missingTextString = "Localized text not found!";
	
	// Use this for creating the instance
	void Awake () {
		if ( instance == null )
		{
			instance = this;
		}
		else if( instance != null )
		{
			Destroy( gameObject );
		}
		DontDestroyOnLoad( gameObject );
		
		BetterStreamingAssets.Initialize(  );
	}

	public void LoadLocalizedText( string fileName )
	{
		// VERSION ACTUALIZADA UTILIZANDO C# DIRECTAMENTE
		// Version de Unity en el link https://unity3d.com/es/learn/tutorials/topics/scripting/localized-text-editor-script?playlist=17117
		if ( !BetterStreamingAssets.FileExists(fileName) )
		{
			Debug.LogErrorFormat("Streaming asset not found: {0}", fileName);
		}

		var dataAsJson = BetterStreamingAssets.OpenText( fileName );
		var jsontest = dataAsJson.ReadToEnd( );
		localizedText = new Dictionary< string, string >();
		LocalizationData loadedData = JsonUtility.FromJson< LocalizationData >( jsontest );
		
		for ( int i = 0; i < loadedData.items.Length; i++ )
		{
			localizedText.Add( loadedData.items[i].key, loadedData.items[i].value );
		}

		isReady = true;
	}
	
	/*
	IEnumerator LoadLocalizedTextOnAndroid (string fileName)
	{
		localizedText = new Dictionary<string, string> ();
		string filePath;// = Path.Combine(Application.streamingAssetsPath, fileName);
		//filePath = Path.Combine (Application.streamingAssetsPath + "/", fileName);
		filePath = Application.streamingAssetsPath + fileName;
		//filePath = Path.Combine ("jar:file://" + Application.dataPath + "!/assets/", fileName);
		
		string dataAsJson;
		
		if (filePath.Contains ("://") || filePath.Contains (":///")) {
			//debugText.text += System.Environment.NewLine + filePath;
			Debug.Log ("UNITY:" + System.Environment.NewLine + filePath);
			UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get (filePath);
			yield return www.Send ();
			dataAsJson = www.downloadHandler.text;
		} else {
			dataAsJson = File.ReadAllText (filePath);
		}
		LocalizationData loadedData = JsonUtility.FromJson<LocalizationData> (dataAsJson);

		for (int i = 0; i < loadedData.items.Length; i++) {
			localizedText.Add (loadedData.items [i].key, loadedData.items [i].value);
			Debug.Log ("KEYS:" + loadedData.items [i].key);
		}

		isReady = true;
	}*/

	// En principio esto no se utiliza, ya que no hacemos uso de directivas.
	public void SetLanguage( string fileName )
	{
		#if UNITY_EDITOR
		//LoadLocalizedText( fileName );
		LoadLocalizedText( fileName );
		#elif UNITY_ANDROID
		Debug.Log("Unity Android");
		LoadLocalizedText( fileName );
		#endif
	}

	public string GetLocalizedValue( string key )
	{
		string result = missingTextString;
		if ( localizedText.ContainsKey( key ) )
		{
			result = localizedText[ key ];
			//Debug.Log( result );
		}

		return result;
	}

	public bool GetIsReady( )
	{
		return isReady;
	}
}
