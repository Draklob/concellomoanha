﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MonumentDisplay : MonoBehaviour
{
	public Monument monument;

	public TextMeshProUGUI titleText;
	public TextMeshProUGUI descriptionText;

	public Image portraitImage;

	private GameObject monument3D;

	private void Start( )
	{
		titleText.SetText( monument.title ); 
		descriptionText.SetText( monument.description );

		portraitImage.sprite = monument.portrait;
		monument3D = monument.monument3D;
	}

	public void SeeMonumentInfo( )
	{
		//MuseumScreen.Show( monument3D );
		//InfoMonumentScreen.Show( monument );
	}
}
