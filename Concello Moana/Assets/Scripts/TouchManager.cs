﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {

	public float speed = 5.0f;
	public float speedRotation = 40.0f;
	
	public GameObject rootCamera;
	public Camera camera3D;
	
	private GameObject gObj;
	private Plane objPlane;
	private Vector3 mO;
	private float lastDistance;
	
	private float minRotationY = -86f;
	private float maxRotationY = 86f;
	private Quaternion currentRotation;
	private Quaternion desiredRotation;
	private Quaternion rotation;
	private float currentMainRotation;
	private float currentMainRotationX;
	
	// Use this for initialization
	void Start () {
		rotation = rootCamera.transform.rotation;
		currentRotation = rootCamera.transform.rotation;
	}
	
	// Update is called once per frame
	void LateUpdate( )
	{
		#if UNITY_EDITOR
			if ( Input.GetMouseButton( 0 ) )
			{
				RotacionAxis( );
				
				/*		// ESTA IDEA PARECE INTERESANTE USANDO GRADOS
			float rotX = Input.GetAxis( "Mouse X" )* 20.0f * Mathf.Deg2Rad;
			float rotY = Input.GetAxis( "Mouse Y" )* 20.0f * Mathf.Deg2Rad;
			
			modelo.transform.Rotate( Vector3.up, -rotX );
			modelo.transform.Rotate( Vector3.right, rotY );
			*/
				
			}
			else if ( Input.GetMouseButton( 1 ) )
			{
				MoveObject(  );
			}
			else if ( Input.GetAxis( "Mouse ScrollWheel" ) != 0 )
			{
				camera3D.gameObject.GetComponent<Transform>(  ).transform.Translate( Vector3.back * Input.GetAxis("Mouse ScrollWheel") );
				
			}
		#endif
		
		#if UNITY_ANDROID
			if( Input.touchCount > 1 )
			{
				if ( Input.GetTouch( 0 ).phase == TouchPhase.Moved && Input.GetTouch( 1 ).phase == TouchPhase.Moved)
				{
					 
					float distance = Vector3.Distance( Input.GetTouch( 0 ).position, Input.GetTouch( 1 ).position );
					//distanceText.text = distance.ToString( );
					//currentPos.text = lastDistance.ToString();
	
					if ( lastDistance == 0.0f )
						lastDistance = distance;
					else
					{
						if ( distance > lastDistance + 2f )
						{
							camera3D.gameObject.GetComponent<Transform>(  ).transform.Translate( Vector3.forward * 5.0f * Time.deltaTime );
							//modelo.transform.Translate( Vector3.back * 5.0f * Time.deltaTime);
						}
						else if ( distance < lastDistance - 2f )
						{
							camera3D.gameObject.GetComponent<Transform>(  ).transform.Translate( Vector3.back * 5.0f * Time.deltaTime );

							//modelo.transform.Translate( Vector3.forward * 5.0f * Time.deltaTime);
						}
						else
						{
							MoveObject(  );
						}
		
						lastDistance = distance;
					}
				}
			}
			else //( Input.touchCount > 0 && Input.touchCount < 1 )
			{
				if ( Input.GetTouch( 0 ).phase == TouchPhase.Began )
				{
					//tCount.text = Input.GetTouch( 0 ).position.ToString();
					//tCount.text = subCamera.ScreenToWorldPoint( Input.GetTouch( 0 ).position ).ToString( );
				}
				else if ( Input.GetTouch( 0 ).phase == TouchPhase.Moved )
				{
					RotacionAxis(  );
				}
			}
		#endif
	}

	private void MoveObject( )
	{
		Vector2 movMouse = new Vector2( Input.GetAxis( "Mouse X" ), Input.GetAxis( "Mouse Y" ) ) * -2 * Time.deltaTime;
		//currentPos.text = movMouse.ToString();
		rootCamera.transform.Translate( movMouse.x, movMouse.y, 0 );
	}
	
	private void RotacionAxis( )
	{
		float rotationX = Input.GetAxis( "Mouse X" ) * speedRotation * Time.deltaTime;
		float rotationY = Input.GetAxis( "Mouse Y" ) * speedRotation * Time.deltaTime;

		currentMainRotation = Mathf.Clamp( currentMainRotation + rotationY, minRotationY, maxRotationY );
		currentMainRotationX = currentMainRotationX + rotationX;
		rootCamera.transform.rotation = Quaternion.identity;
		rootCamera.transform.Rotate( Vector3.up, currentMainRotationX );
		rootCamera.transform.Rotate( Vector3.left, currentMainRotation );
	
		// Move the model with the axis we got previously
		//modelo.transform.Rotate( rotationX, rotationY, 0.0f );
		//modelo.transform.eulerAngles = new Vector3( rotationY, rotationX, 0.0f );

		/*
		rotationY = ClampAngle( rotationY, -120, 120 );
		desiredRotation = Quaternion.Euler( rotationY, rotationX, 0 );
		currentRotation = modelo.transform.rotation;
		
		rotation = Quaternion.Lerp( currentRotation, desiredRotation, Time.deltaTime );
		modelo.transform.rotation = rotation;
		*/
	}
}
