﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoMonumentScreen : Menu<InfoMonumentScreen>
{

	public Image background;
	public TextMeshProUGUI titleText;
	public TextMeshProUGUI descriptionText;
	
	private Monument monumento;
	private MonumentDisplay[ ] listaMonumentos;
	
	private int currentMonument;
	private int lastLoadedMonument;
	
	public Button buttonGoTop;
	private bool goingTop;
	Vector2 startPosition = Vector2.zero;
	private float smoothingTime;
	
	public static void Show( Monument _monument, MonumentDisplay[] _listMonuments)
	{
		Open();
		Instance.monumento = _monument;
		Instance.listaMonumentos = _listMonuments;
	}

	private void Start( )
	{
		Mask _mask = transform.Find( "Panel" ).GetComponent< Mask >( );
		if ( _mask != null )
		{
			if ( !_mask.enabled )
				_mask.enabled = true;
		}
		
		
//		if ( monumentoPrefab == null )
//			return;

		currentMonument = monumento.ID;
		
		LoadScreen( );
	}

	private void Update( )
	{
        
		if( goingTop)
			GoingToTop(  );
	}

	public void PreviousMonument( )
	{
		lastLoadedMonument = currentMonument;
		currentMonument--;
		if ( currentMonument < 0 )
		{
			currentMonument = 0;
		}

		if ( currentMonument != lastLoadedMonument )
			LoadScreen(  );
	}
	
	public void NextMonument( )
	{
		lastLoadedMonument = currentMonument;
		currentMonument++;
		if ( currentMonument > listaMonumentos.Length - 1 )
		{
			currentMonument = listaMonumentos.Length - 1;
		}

		if ( currentMonument != lastLoadedMonument )
			LoadScreen(  );
	}
/*
	private void LoadScreen( )
	{
		titleText.SetText( listaElementsToShow[ currentScreen ].titleText );

		if ( subTexts.Length >= 1 )
		{
			for ( int i = 0; i < subTexts.Length; i++ )
			{
				subTexts[i].SetText( listaElementsToShow[currentScreen].subTexts[i] );
			}
		}

		if ( subImages.Length >= 1 )
		{
			for ( int i = 0; i < subImages.Length; i++ )
			{
				subImages[ i ].sprite = listaElementsToShow[ currentScreen ].subImages[ i ];
			}
		}
	}
*/
	private void LoadScreen()
	{

		//background.sprite = monumentoPrefab.portraitInfo;
		background.sprite = listaMonumentos[currentMonument].monument.portraitInfo;
		//titleText.SetText( monumentoPrefab.title );
		titleText.SetText(listaMonumentos[currentMonument].monument.title);
		descriptionText.SetText(listaMonumentos[currentMonument].monument.textInfo);

		Tools.UpdatingCanvas(transform.Find("Panel/Content").GetComponent<VerticalLayoutGroup>());
	}


	public void ToggleGoingTop( )
	{
		goingTop = !goingTop;
	}
    
	private void GoingToTop( )
	{
		RectTransform content = transform.Find( "Panel/Content" ).GetComponent<RectTransform>(  );

		if ( startPosition == Vector2.zero )
		{
			startPosition = content.anchoredPosition;
			Debug.Log( startPosition );
            
		}

		smoothingTime += Time.deltaTime * 2;
        
		content.anchoredPosition =
			Vector2.Lerp( new Vector2( 0f, 3331f), new Vector2( 0f, 0f ), smoothingTime );

		if ( smoothingTime > 1f )
		{
			Debug.Log( "EN EL TOP" );
			goingTop = false;
			smoothingTime = 0;
		}
        
	}

	public void SeeMonument3D( )
	{
		MuseumScreen.Show( monumento );
	}
}
