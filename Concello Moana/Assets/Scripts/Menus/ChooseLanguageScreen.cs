﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseLanguageScreen : SimpleMenu<ChooseLanguageScreen>
{
    public LocalizationManager _localizationManager;

    protected override void Awake( )
    {
        base.Awake(  );
        // Disables fullscreen mode after showing the splash screen.
        Tools.FullScreenMode(  );
    }

    private void Start( )
    {
         _localizationManager = LocalizationManager.instance;
    }

    public void OnGalegoButtonPressed( )
    {
        CloseScreen(  );
        MainMenu.Show(  );
        _localizationManager.LoadLocalizedText( "localizedText_ga.json" );
    }

    public void OnCastellanoButtonPressed( )
    {
        MainMenu.Show(  );
        _localizationManager.LoadLocalizedText( "localizedText_es.json" );
        //CloseScreen( );
    }

    public void OnEnglishButtonPressed( )
    {
        MainMenu.Show(  );
       _localizationManager.LoadLocalizedText( "localizedText_en.json" );
        //CloseScreen( );
    }

    private void CloseScreen( )
    {
        //Destroy( gameObject );
        Close(  );
    }
}
