﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : SimpleMenu<MainMenu> {
	private void Start( )
	{
		Debug.Log( Instance );
	}

	public void OnPatrimonioButtonPressed()
    {
	    //Hide(  );
	    PatrimonioCulturaScreen.Show( );
    }
	
	public void OnTurismoButtonPressed()
	{
        TurismoScreen.Show(  );
	}
	
	public void OnConcelloButtonPressed()
	{
        ConcelloScreen.Show(  );
	}
	
	public void OnLibroElectronicoButtonPressed()
	{
		Close(  );

		SceneManager.LoadScene( "LibroElectronico" );
	}

    public override void OnBackPressed()
	{
		Application.Quit();
	}
}
