﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PhotoMemoryScreen : Menu<PhotoMemoryScreen>
{
    public Image photo;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI autorText;

    public GameObject portraitPanel;
    public GameObject landscapePanel;

    public bool landscapeMode;
    
    private MemoriaFotografica memoriaFotografica;
    private MemoriaFotograficaDisplay[ ] listaDeFotos;

    private int currentPhoto;
    private int lastPhoto;

    private CanvasScaler canvasScaler;

    public static void Show( MemoriaFotograficaDisplay[] _listaDeFotos, int IDCurrentPhoto )
    {
        Open(  );

        Instance.listaDeFotos = _listaDeFotos;

        Instance.currentPhoto = IDCurrentPhoto;

    }
    
    // Start is called before the first frame update
    void Start()
    {
        LoadScreen(  );

        //Tools.FullScreenMode(  );
        
        canvasScaler = GetComponent< CanvasScaler >( );
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void PreviousPhoto( )
    {
        lastPhoto = currentPhoto;
        currentPhoto--;
        if ( currentPhoto < 0 )
        {
            currentPhoto = 0;
        }

        if ( currentPhoto != lastPhoto )
            LoadScreen(  );
    }
	
    public void NextPhoto( )
    {
        lastPhoto = currentPhoto;
        currentPhoto++;
        if ( currentPhoto > listaDeFotos.Length - 1 )
        {
            currentPhoto = listaDeFotos.Length - 1;
        }

        if ( currentPhoto != lastPhoto )
            LoadScreen(  );
    }

    private void LoadScreen( )
    {
        //SwapOrientation( listaDeFotos[currentPhoto] );
        Debug.Log( Camera.main.aspect);

        SwapOrientation( );
        memoriaFotografica = listaDeFotos[ currentPhoto ].memoriaFotografica;

        landscapeMode = memoriaFotografica.landscapePhoto;
        
        
        photo.sprite = memoriaFotografica.portraitPhoto;
        titleText.SetText( memoriaFotografica.title );
        autorText.SetText( memoriaFotografica.autor );

//        Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );
        // Settting LocalPosition for BoxTrigger
        //Invoke( nameof( SettingPositionTrigger), 0.1f );
    }

    private void SwapOrientation( )
    {
        //canvasScaler.referenceResolution = new Vector2( Screen.width, Screen.height );
        RectTransform transform = this.transform.Find( "Panel" ).GetComponent< RectTransform >( );
        Debug.Log( transform.offsetMax + ", " + transform.offsetMin );

        if ( landscapeMode )
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            transform.offsetMin = new Vector2( 0, 0);
            transform.offsetMax = new Vector2( -126, -50);
        }
        else
        {
            Screen.orientation = ScreenOrientation.Portrait;
            transform.offsetMin = new Vector2( 0, 126);
            transform.offsetMax = new Vector2( 0, -63);
        }
    }
    
    public override void OnBackPressed()
    {
        //Tools.FullScreenMode(  );
        Screen.orientation = ScreenOrientation.Portrait;
        
        
        base.OnBackPressed(  );
    }
}
