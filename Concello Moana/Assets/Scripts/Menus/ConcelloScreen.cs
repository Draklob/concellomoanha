﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcelloScreen : SimpleMenu<ConcelloScreen>
{
    public HistoriaELendas[ ] listaMar;
    public HistoriaELendas[ ] listaFestas;
    public HistoriaELendas[ ] listaDeporte;


    public void OnWebButtonPressed( string url)
    {
	    Application.OpenURL( url );
    }
	
    public void OnLigadoMarButtonPressed()
    {
	    InfoHistoriaScreen.Show( listaMar);
    }
	
    public void OnFestasButtonPressed()
    {
	    InfoHistoriaScreen.Show( listaFestas);
    }
	
    public void OnDeporteButtonPressed()
    {
        InfoHistoriaScreen.Show( listaDeporte);
    }
}
