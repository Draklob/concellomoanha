﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurismoScreen : SimpleMenu<TurismoScreen> {

	public HistoriaELendas[ ] listaEventos;
	
	public string keyword;
	
    public void OnRutasButtonPressed()
    {
	    RutasScreen.Show(  );
    }
	
    public void OnGastronomiaButtonPressed( string keywordPassed)
    {
	    keyword = keywordPassed;
	    GastronomiaScreen.Show();
    }
	
    public void OnPraiasButtonPressed()
    {
        PraiasScreen.Show(  );
    }
	
    public void OnEventosButtonPressed()
    {
        InfoHistoriaScreen.Show( listaEventos);
    }
}
