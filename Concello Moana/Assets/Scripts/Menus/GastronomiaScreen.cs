﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GastronomiaScreen : SimpleMenu<GastronomiaScreen>
{
    
    public List<Item> items = new List< Item >();
    
    private List<Texture2D> fotosGoogle = new List<Texture2D>();
    private string keyword;
    private string referencePhoto;
    
    // Start is called before the first frame update
    void Start()
    {
        if ( TurismoScreen.Instance.keyword != null )
        {
            keyword = TurismoScreen.Instance.keyword;
            //StartCoroutine( MakeURLRequest( ) );
            //StartCoroutine( RequestImageFromWeb( ) );
            CreateList(  );
            //ReadingList(  );
        }
    }
    
    IEnumerator MakeURLRequest( )
    {
        //string url = "https://www.buildandrun.tv/GPSCourse/parksJson.json";
        string url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=42.274495, -8.737189&radius=500" +
                     "&type=" + keyword +"&key=AIzaSyALJA0DMFmad6hluT7xzynuPnRyHcAoCSo";

        using ( UnityWebRequest uwr = UnityWebRequest.Get( url ) )
        {
            yield return uwr.SendWebRequest(  );
            if( uwr.isNetworkError || uwr.isHttpError )
                Debug.Log( uwr.error  );
            else
            {
                SaveJsonToFile( uwr.downloadHandler.text );
                CreateList( );    
            }
        }
    }
    
    IEnumerator RequestImageFromWeb( string _referenPhoto, int index)
    {
        if ( Application.internetReachability == NetworkReachability.NotReachable )
            yield return null; // NO ES SEGURO DE QUE ESTO FUNCIONE

        if (_referenPhoto == null)
            yield return null;    // DEBERIA SALIRSE SI NO TENEMOS UNA REFERENCIA DE LA FOTO O SE PODIA BUSCAR PONER UNA POR DEFECTO

        referencePhoto = _referenPhoto;
        
        string   url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400" +
                       "&photoreference=" + referencePhoto +
                       "&key=AIzaSyALJA0DMFmad6hluT7xzynuPnRyHcAoCSo";
        
        using ( UnityWebRequest uwr = UnityWebRequestTexture.GetTexture( url ) )
        {
            yield return uwr.SendWebRequest(  );
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                Debug.Log( "Cargando Imagen" );
                fotosGoogle.Add(DownloadHandlerTexture.GetContent( uwr ));
                CreateList( );    

            }
        }
    }
    
    private void SaveJsonToFile( string content )
    {
        string path = Application.dataPath + "/Log.json";
        
        // Create the file if it doesn't exist
        if ( !File.Exists( path ) )
        {
            Debug.Log( "Creando archivo JSON" );
            File.WriteAllText( path, content );
        }
    }

    private string LoadJson( )
    {
        string path = Application.dataPath + "/Log.json";

        if ( File.Exists( path ) )
        {
            string jsonString = File.ReadAllText( path );
            return jsonString;
        }

        return null;
    }

    public void CreateList()
    {
        string jsonString = LoadJson( );
        
        if( jsonString != null )
        {
            RootObject thePlaces = new RootObject(  );
            Newtonsoft.Json.JsonConvert.PopulateObject( jsonString, thePlaces );
//            Debug.Log( thePlaces.results[0].name );
//            Debug.Log( thePlaces.results[0].vicinity );
//            Debug.Log( thePlaces.results[0].geometry.location.lat );
//            Debug.Log( thePlaces.results[0].geometry.location.lng );   
//            Debug.Log( jsonString );
//            Debug.Log( thePlaces.results.Count );

            if ( thePlaces.results.Count > 0 )
            {
                for ( int i = 0; i < thePlaces.results.Count; i++ )
                {
                    // Calculcamos la distancia
                    int distance = (int)Tools.GeoCodeCalc.CalcDistance(42.274495, -8.737189,
                        thePlaces.results[i].geometry.location.lat, thePlaces.results[i].geometry.location.lng, Tools.GeoCodeCalcMeasurement.Metre );
            //        float distance = (float)Tools.GeoCodeCalc.CalcDistance(GPS.latitude, GPS.longitude,
             //           thePlaces.results[i].geometry.location.lat, thePlaces.results[i].geometry.location.lng, Tools.GeoCodeCalcMeasurement.Metre );



                    if (thePlaces.results[i].rating < 3)
                    {
                        continue;
                    }

       /*             var photos = thePlaces.results[i].photos;
                    if( photos != null)
                        StartCoroutine(RequestImageFromWeb(photos[0].photo_reference, i));

                    Debug.Log( fotosGoogle.Count);
                    foreach (var foto in fotosGoogle)
                    {
                        Debug.Log( foto.name);
                    }
          */          
                    // Creamos la lista
                    var hours = thePlaces.results[i].opening_hours;
                    items.Add( new Item( thePlaces.results[i].name, distance, thePlaces.results[i].rating, hours != null && hours.open_now  ) );
                }
            }
            
            ReadingList(  );

            foreach ( var item in items )
            {
                GameObject placePrefab = Instantiate( Resources.Load( "RestaurantPrefab" ) ) as GameObject;
                GameObject contentHolder = GameObject.FindGameObjectWithTag( "Content" );
                placePrefab.transform.SetParent( contentHolder.transform, false );

                TextMeshProUGUI[ ] texts = placePrefab.GetComponentsInChildren< TextMeshProUGUI >( );
                texts[ 0 ].text = item.Name;
                texts[ 1 ].text = item.Status ? "Abierto" : "Cerrado";
                texts[ 2 ].text = "Puntuacion: " + item.Rating;
                texts[ 3 ].text = "Distancia: " + item.Distance + "m";
                //texts[ 4 ].text = item.TotalReviews + " opiniones";
                // EL CAMBIO ES QUE DESPUES CAMBIAMOS LAS COORDENADAS PUESTAS A MANO POR LA LOCALIZACION DEL USUARIO GPS.latitude GPS.longitude

                Image[ ] fotos = placePrefab.GetComponentsInChildren< Image >( );
                //Debug.Log( fotos[1].gameObject.name );
                
        //            fotos[1].sprite = Sprite.Create( item.Textura, new Rect(0,0,item.Textura.width,item.Textura.height), 
        //                new Vector2( 0.5f, 0.5f), 100.0f  );
                        
                //img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            }
        }
    }

    public void ReadingList( )
    {
        // Sin ordenar
        /* 
        foreach ( var item in items )
        {
            Debug.Log( item.Name + ", con puntuacion de " + item.Rating + ", a " + item.Distance + "m metros, con "
                       + item.TotalReviews + " de reviews y ahora su estado esta " + (item.Status?"abierto":"cerrado"));
            //Debug.Log( item.Rating );
            
        }*/
        
        items.Sort( Comparison);
        
     /*   foreach ( var item in items )
        {
            Debug.Log( item.Name + ", con puntuacion de " + item.Rating + ", a " + item.Distance + "m metros, con "
                       + item.TotalReviews + " de reviews y ahora su estado esta " + (item.Status?"abierto":"cerrado"));          
        }*/
    }

    private int Comparison( Item a, Item b )
    {
        if ( a.Distance < b.Distance )
            return -1;
        else if( a.Distance > b.Distance)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

}

public class Item
{
    public string Name;
    public float Distance;
    public double Rating;
    public int TotalReviews;
    public bool Status;
    public string ReferencePhoto;
    public Texture2D Textura;

    public Item( string name, float distance, double rating )
    {
        Name = name;
        Distance = distance;
        Rating = rating;
    }
    
    public Item( string name, float distance, double rating, bool status )
    {
        Name = name;
        Distance = distance;
        Rating = rating;
        Status = status;
    }
    
    public Item( string name, float distance, double rating, bool status, Texture2D textura )
    {
        Name = name;
        Distance = distance;
        Rating = rating;
        Status = status;
        Textura = textura;
    }

    public Item( string name, float distance, double rating, int totalReviews, bool status, string referenPhoto )
    {
        Name = name;
        Distance = distance;
        Rating = rating;
        TotalReviews = totalReviews;
        Status = status;
        ReferencePhoto = referenPhoto;
    }
}

public class Location
{
    public double lat { get; set; }
    public double lng { get; set; }
}

public class Northeast
{
    public double lat { get; set; }
    public double lng { get; set; }
}

public class Southwest
{
    public double lat { get; set; }
    public double lng { get; set; }
}

public class Viewport
{
    public Northeast northeast { get; set; }
    public Southwest southwest { get; set; }
}

public class Geometry
{
    public Location location { get; set; }
    public Viewport viewport { get; set; }
}

public class OpeningHours
{
    public bool open_now { get; set; }
}

public class Photo
{
    public int height { get; set; }
    public List<string> html_attributions { get; set; }
    public string photo_reference { get; set; }
    public int width { get; set; }
}

public class PlusCode
{
    public string compound_code { get; set; }
    public string global_code { get; set; }
}

public class Result
{
    public Geometry geometry { get; set; }
    public string icon { get; set; }
    public string id { get; set; }
    public string name { get; set; }
    public OpeningHours opening_hours { get; set; }
    public List<Photo> photos { get; set; }
    public string place_id { get; set; }
    public PlusCode plus_code { get; set; }
    public double rating { get; set; }
    public string reference { get; set; }
    public string scope { get; set; }
    public List<string> types { get; set; }
    public int user_ratings_total { get; set; }
    public string vicinity { get; set; }
    public int? price_level { get; set; }
}

public class RootObject
{
    public List<object> html_attributions { get; set; }
    public string next_page_token { get; set; }
    public List<Result> results { get; set; }
    public string status { get; set; }
}

