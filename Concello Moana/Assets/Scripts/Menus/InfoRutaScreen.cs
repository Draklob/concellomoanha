﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoRutaScreen : Menu<InfoRutaScreen>
{
    
    private HistoriaELendas[ ] listaRutas;

    private int currentRuta;
    private int lastRuta;
    
    public Image background;
    public TextMeshProUGUI titleText;
    public Image featureList;
    public TextMeshProUGUI descriptionText;
    
    public static void Show(HistoriaELendas[ ] _listaDeRutas, int ruta )
    {
        Open(  );
        Instance.listaRutas = _listaDeRutas;
        Instance.currentRuta = ruta;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        LoadScreen( );
    }

    public void NextScreen( )
    {
        lastRuta = currentRuta;
        currentRuta++;
        if ( currentRuta > listaRutas.Length - 1 )
        {
            currentRuta = listaRutas.Length - 1;
        }

        if ( currentRuta != lastRuta )
            LoadScreen(  );
    }

    public void PreviousScreen( )
    {
        lastRuta = currentRuta;
        currentRuta--;
        if ( currentRuta < 0 )
        {
            currentRuta = 0;
        }

        if ( currentRuta != lastRuta )
            LoadScreen(  );
    }
   
    private void LoadScreen( )
    {
        //background.sprite = monumentoPrefab.portraitInfo;
        background.sprite = listaRutas[ currentRuta ].background;
        //titleText.SetText( monumentoPrefab.title );
        titleText.SetText( listaRutas[ currentRuta ].titleText );
        descriptionText.SetText( listaRutas[ currentRuta ].subTexts[0] );
        featureList.sprite = listaRutas[ currentRuta ].subImages[0] ;

        Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );
    }
}
