﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryScreen : Menu<GalleryScreen>
{

	public MonumentDisplay[ ] listaDeMonumentos;

	public static void Show()
	{
		Open();
	}

	private void Start( )
	{
		for ( int i = 0; i < listaDeMonumentos.Length; i++ )
		{
			listaDeMonumentos[ i ].monument.ID = i;
		}

	}

	public void OnMonumentSelectedButtonPressed( MonumentDisplay _monument)
	{
		InfoMonumentScreen.Show(_monument.monument, listaDeMonumentos);
	}
	
	
	
	// Update is called once per frame
	void Update () {
		
	}
}
