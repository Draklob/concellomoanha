﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrimonioCulturaScreen : SimpleMenu<PatrimonioCulturaScreen> {
	public void OnMuseoVirtualButtonPressed()
	{
        GalleryScreen.Show(  );
	}
	
	public void OnHistoriaButtonPressed()
	{
        HistoriaScreen.Show(  );
	}
}
