﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MemoryPhotoGallery_Screen : SimpleMenu<MemoryPhotoGallery_Screen>
{
    public MemoriaFotograficaDisplay[ ] listaFotosAntiguas;

    // Start is called before the first frame update
    void Start()
    {
        for ( int i = 0; i < listaFotosAntiguas.Length; i++ )
        {
            listaFotosAntiguas[ i ].memoriaFotografica.ID = i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnSeePhotoGallery( int IDNumber )
    {
        PhotoMemoryScreen.Show( listaFotosAntiguas, IDNumber);
    }
}
