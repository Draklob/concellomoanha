﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoriaScreen : SimpleMenu<HistoriaScreen>
{

	public HistoriaELendas[ ] LendasMitos;
	
	public void OnHistoriaGeneralButtonPressed( )
	{
		HistoriaGeneralScreen.Show(  );
	}
	
	public void OnMemoriaFotograficaButtonPressed( )
	{
		MemoryPhotoGallery_Screen.Show(  );
	}
	
	public void OnLendasMitosButtonPressed( )
	{
		InfoHistoriaScreen.Show( LendasMitos );
	}
	
}
