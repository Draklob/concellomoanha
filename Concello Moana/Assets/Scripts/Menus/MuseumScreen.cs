﻿using UnityEngine;

public class MuseumScreen : Menu<MuseumScreen>
{
	private Monument monumento;
	
	private Camera mainCamera;

	public static void Show( Monument _monument)
	{
		//MenuManager.Instance.museumScreenPrefab.monumentoPrefab = _monument;
		Open();
		Instance.monumento = _monument;
	}
	
	public override void OnBackPressed()
	{
		mainCamera.gameObject.SetActive( true );
		base.OnBackPressed(  );
	}
	
	private void Start( )
	{
		mainCamera = GameObject.FindWithTag( "MainCamera" ).GetComponent<Camera>(  );
		mainCamera.gameObject.SetActive( false );

		if ( !monumento )
			return;

		Camera camera3D = GetComponent< TouchManager >( ).camera3D;
		camera3D.gameObject.transform.localPosition = new Vector3( 0, 0, monumento.initialZoom * -1);
		GameObject rootCamera = GetComponent< TouchManager >( ).rootCamera;
		
	
		Vector3 centerModel = monumento.monument3D.GetComponentInChildren< Renderer >( ).bounds.center;
		//Instantiate( monumentoPrefab, new Vector3( 0.54f, -0.41f, 5f), Quaternion.identity, transform );
		Instantiate( monumento.monument3D, new Vector3( centerModel.x, centerModel.y / 2, 0), Quaternion.identity, transform );
		rootCamera.transform.position = centerModel;
		//Instantiate( monumentoPrefab, Vector3.zero - new Vector3( centerModel.z, centerModel.y, 0), Quaternion.identity, transform );

	}
}
