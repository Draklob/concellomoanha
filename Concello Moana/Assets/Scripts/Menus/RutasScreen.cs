﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RutasScreen : SimpleMenu<RutasScreen>
{
    public HistoriaELendas[ ] listaRutas;

    private void Start( )
    {
        Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );

        for ( int i = 0; i < listaRutas.Length; i++ )
        {
            listaRutas[ i ].ID= i;
        }
    }
    
    public void OnInfoRutaButtonPressed( int IDRuta )
    {        
        InfoPraiaScreen.Show( listaRutas, IDRuta );
    }
}
