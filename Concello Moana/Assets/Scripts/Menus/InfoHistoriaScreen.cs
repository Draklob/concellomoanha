﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoHistoriaScreen : Menu<InfoHistoriaScreen>
{
    public Image background;
    public TextMeshProUGUI titleText;
    
    public TextMeshProUGUI textPrefab;
    public Image imagePrefab;

    private HistoriaELendas[ ] listaElementsToShow;
    private int currentScreen;
    private int lastLoadedScreen;

    public Button buttonGoTop;
    private bool goingTop;
    Vector2 startPosition = Vector2.zero;
    private float smoothingTime;

    private List< GameObject > listOfTexts = new List< GameObject >();
    private List< GameObject > listOfImagess = new List< GameObject >();
    
    public static void Show( HistoriaELendas[ ] _elementsToShow)
    {
        Open();
        Instance.listaElementsToShow = _elementsToShow;
    }

    // Start is called before the first frame update
    void Start()
    {
        Mask _mask = transform.Find( "Panel" ).GetComponent< Mask >( );
        if ( _mask != null )
        {
            if ( !_mask.enabled )
                _mask.enabled = true;
        }
        
        LoadScreen(  );
    }

    private void Update( )
    {
//        if ( Tools.IsVisible( boxTrigger ) )
//        {
//            buttonGoTop.gameObject.SetActive( true );
//        }
//        else
//            buttonGoTop.gameObject.SetActive( false );
        
        if( goingTop)
            GoingToTop(  );
            
    }

    public void NextScreen( )
    {
        lastLoadedScreen = currentScreen;
        currentScreen++;
        if ( currentScreen > listaElementsToShow.Length - 1 )
        {
            currentScreen = listaElementsToShow.Length - 1;
        }

        if ( currentScreen != lastLoadedScreen )
            LoadScreen(  );
    }

    public void PreviousScreen( )
    {
        lastLoadedScreen = currentScreen;
        currentScreen--;
        if ( currentScreen < 0 )
        {
            currentScreen = 0;
        }

        if ( currentScreen != lastLoadedScreen )
            LoadScreen(  );
    }

    private void LoadScreen( )
    {
        background.sprite = listaElementsToShow[ currentScreen ].background;
        titleText.SetText( listaElementsToShow[ currentScreen ].titleText );

        // Eliminamos los textos de la pantalla anterior y los volvemos a crear despues
        if ( listOfTexts.Count > 0 )
        {
            foreach ( var go in listOfTexts )
            {
                Destroy( go );
            }
            
            listOfTexts.Clear(  );
        }
        
        // Eliminamos las imagenes de la pantalla anterior y los volvemos a crear despues
        if ( listOfImagess.Count > 0 )
        {
            foreach ( var go in listOfImagess )
            {
                Destroy( go );
            }
            
            listOfImagess.Clear(  );
        }

        int j = 0;
        if ( listaElementsToShow[currentScreen].subTexts.Length >= 1 )
        {
            for ( int i = 0; i < listaElementsToShow[currentScreen].subTexts.Length; i++ )
            {
                textPrefab.text = listaElementsToShow[currentScreen].subTexts[i];
                TextMeshProUGUI textPrefInstan = Instantiate( textPrefab, transform.Find( "Panel/Content" ) );
                listOfTexts.Add( textPrefInstan.gameObject );
                //subTexts[i].SetText( listaElementsToShow[currentScreen].subTexts[i] );

                if( j < listaElementsToShow[ currentScreen ].subImages.Length )
                {
                    imagePrefab.sprite = listaElementsToShow[ currentScreen ].subImages[ j ]; 
                    Image tempImage = Instantiate( imagePrefab, transform.Find( "Panel/Content" ) );
                    listOfImagess.Add( tempImage.gameObject );
                    j++;
                }

                while ( listaElementsToShow[currentScreen].subTexts.Length <= j && j < listaElementsToShow[ currentScreen ].subImages.Length )
                {
                    imagePrefab.sprite = listaElementsToShow[ currentScreen ].subImages[ j ]; 
                    Image tempImage = Instantiate( imagePrefab, transform.Find( "Panel/Content" ) );
                    listOfImagess.Add( tempImage.gameObject );
                    j++;
                }
            }
        }
        
        // Update canvas after load everything
        Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );
        // Settting LocalPosition for BoxTrigger
        Invoke( nameof( SettingGoingTopButton), 0.1f );
    }
    
    private void SettingGoingTopButton( )
    {
        float heightContent = transform.Find( "Panel/Content" ).GetComponent< RectTransform >( ).rect.height;
        //Transform trigger = boxTrigger.GetComponent< Transform >( ).transform;
        buttonGoTop.transform.localPosition = new Vector3( 1020f, (heightContent - 60) * -1, 0f);
        //trigger.localPosition = new Vector3( trigger.localPosition.x,(heightContent - 60 ) * -1, trigger.localPosition.z );
    }
   
    public void ToggleGoingTop( )
    {
        goingTop = !goingTop;
    }
    
    private void GoingToTop( )
    {
        RectTransform content = transform.Find( "Panel/Content" ).GetComponent<RectTransform>(  );

        if ( startPosition == Vector2.zero )
        {
            startPosition = content.anchoredPosition;
            Debug.Log( startPosition );
            
        }

        smoothingTime += Time.deltaTime * 2;
        
        content.anchoredPosition =
            Vector2.Lerp( new Vector2( 0f, 3331f), new Vector2( 0f, 0f ), smoothingTime );

        if ( smoothingTime > 1f )
        {
            Debug.Log( "EN EL TOP" );
            goingTop = false;
            smoothingTime = 0;
        }
        
    }

}
