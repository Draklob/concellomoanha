﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoriaGeneralScreen : SimpleMenu<HistoriaGeneralScreen>
{

    public HistoriaELendas[ ] historiaIdadeAntiga;
    public HistoriaELendas[ ] historiaIdadeMedia;
    public HistoriaELendas[ ] historiaIdadeModerna;
    public HistoriaELendas[ ] historiaIdadeContemporanea;
    
    public void OnIdadeAntigaButtonPressed( )
    {
        InfoHistoriaScreen.Show( historiaIdadeAntiga );
    }
    
    public void OnIdadeMediaButtonPressed( )
    {
        InfoHistoriaScreen.Show( historiaIdadeMedia );
    }
    
    public void OnIdadeModernaButtonPressed( )
    {
        InfoHistoriaScreen.Show( historiaIdadeModerna );
    }
    
    public void OnIdadeContemporaneaButtonPressed( )
    {
        InfoHistoriaScreen.Show( historiaIdadeContemporanea );
    }
}
