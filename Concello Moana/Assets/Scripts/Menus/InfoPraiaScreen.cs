﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPraiaScreen : Menu<InfoPraiaScreen>
{
    private HistoriaELendas[ ] listaDePraias;

    private int currentPraia;
    private int lastPraia;
    
    public Image background;
    public TextMeshProUGUI titleText;
    public Image featureList;
    public TextMeshProUGUI descriptionText;
    
   public static void Show(HistoriaELendas[ ] _listaDePraias, int praia )
    {
        Open(  );
        Instance.listaDePraias = _listaDePraias;
        Instance.currentPraia = praia;
    }

   private void Start( )
   {
       LoadScreen( );
   }
   
   public void NextScreen( )
   {
       lastPraia = currentPraia;
       currentPraia++;
       if ( currentPraia > listaDePraias.Length - 1 )
       {
           currentPraia = listaDePraias.Length - 1;
       }

       if ( currentPraia != lastPraia )
           LoadScreen(  );
   }

   public void PreviousScreen( )
   {
       lastPraia = currentPraia;
       currentPraia--;
       if ( currentPraia < 0 )
       {
           currentPraia = 0;
       }

       if ( currentPraia != lastPraia )
           LoadScreen(  );
   }
   
   private void LoadScreen( )
   {
       //background.sprite = monumentoPrefab.portraitInfo;
       background.sprite = listaDePraias[ currentPraia ].background;
       //titleText.SetText( monumentoPrefab.title );
       titleText.SetText( listaDePraias[ currentPraia ].titleText );
       descriptionText.SetText( listaDePraias[ currentPraia ].subTexts[0] );
       featureList.sprite = listaDePraias[ currentPraia ].subImages[0] ;

       Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );
   }
    
}
