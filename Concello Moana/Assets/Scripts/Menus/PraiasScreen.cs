﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PraiasScreen : SimpleMenu<PraiasScreen>
{
    public HistoriaELendas[ ] listaPraias;
    
    private void Start( )
    {
        Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );

        for ( int i = 0; i < listaPraias.Length; i++ )
        {
            listaPraias[ i ].ID= i;
        }

    }

    public void OnInfoPraiaButtonPressed( int IDPraia )
    {        
        InfoPraiaScreen.Show( listaPraias, IDPraia );
    }
    
}
