﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas3D : MonoBehaviour {

	
	// Use this for initialization
	void Start ()
	{
		Canvas canvas3D = GetComponent< Canvas >( );
		canvas3D.worldCamera = GameObject.FindGameObjectWithTag( "Camera3D" ).GetComponent<Camera>(  );
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
