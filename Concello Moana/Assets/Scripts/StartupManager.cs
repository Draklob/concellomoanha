﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupManager : MonoBehaviour {

	// Podemos poner una imagen de carga despues de elegir idioma, mientras cambia todos los textos, igual esto lleva tiempo, asique se podria usar esta clase
	// para hacer esta funcion. De momento no tiene utilidad porque la app es demasiado small.
	// Link al video. https://www.youtube.com/watch?time_continue=594&v=1dmrhUyCMG4
	
	// Use this for initialization
	private IEnumerator Start () {
		while ( !LocalizationManager.instance.GetIsReady( ) )
		{
			yield return null;
		}
		
		SceneManager.LoadScene( "MainApp" );
	}
}
