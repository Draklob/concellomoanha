﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TestingInstantiate : MonoBehaviour
{
    public GameObject textPro;
    public string textTest;
    
    // Start is called before the first frame update
    void Start()
    {
        
        TextMeshProUGUI tempText = textPro.GetComponent< TextMeshProUGUI >( );
        //tempText.ForceMeshUpdate();

        tempText.text = textTest;
        Instantiate( textPro, transform.Find( "Panel/Content" ) );


//        tempText.GetComponent< TextMeshProUGUI >( ).enabled = false;
//        tempText.GetComponent< TextMeshProUGUI >( ).enabled = true;


        //Tools.UpdatingCanvas( transform.Find( "Panel/Content" ).GetComponent<VerticalLayoutGroup>(  ) );

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
