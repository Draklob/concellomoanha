﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeWithText : MonoBehaviour {

	public void SetNumber( int number )
	{
		GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(  number.ToString( ));
	}

	public void PlayRemoval( )
	{
		GetComponent<Renderer>(  ).material.color = Color.red;
		StartCoroutine( FadeAway( ) );
	}

	private IEnumerator FadeAway( )
	{
		while ( transform.localScale.x > 0 )
		{
			transform.localScale = Vector3.one * ( transform.localScale.x - Time.deltaTime );
			Debug.Log( transform.localScale );
			yield return null;
		}
		Destroy( gameObject );
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
