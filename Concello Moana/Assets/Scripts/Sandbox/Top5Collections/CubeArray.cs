﻿using UnityEngine;

public class CubeArray : MonoBehaviour
{
    [SerializeField] private CubeWithText prefab;

    public int size;
    public const int ARRAY_SIZE = 0;
    [SerializeField]
    private CubeWithText[ ] cubes;

    public GameObject[ ] images = new GameObject[0];

    private int currentCubeNumber;

    private void Awake( )
    {
        //ARRAY_SIZE = size * 8;
        cubes = new CubeWithText[size];
    }

    public void Add( )
    {
        var cube = Instantiate( prefab );
        cube.SetNumber( currentCubeNumber );
        cube.transform.position = transform.position + Vector3.right * currentCubeNumber * 1.10f;
        cube.transform.SetParent( transform );

        cubes[ currentCubeNumber ] = cube;
        currentCubeNumber++;
    }

    public void RemoveLastEntry( )
    {
        currentCubeNumber--;
        var lastCube = cubes[ currentCubeNumber ];
        cubes[ currentCubeNumber ] = null;
        lastCube.PlayRemoval(  );
    }
}
