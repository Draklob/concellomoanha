﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Tools
{
    public static bool IsVisible(Renderer renderer) 
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        if (GeometryUtility.TestPlanesAABB(planes , renderer.bounds))
            return true;
        
        return false;
    }

    public static void UpdatingCanvas( VerticalLayoutGroup _verticalLayout )
    {
        Canvas.ForceUpdateCanvases(); 
        _verticalLayout.enabled = false;
        _verticalLayout.enabled = true;
    }

    public static Vector2 GettingRectSize( RectTransform _transform )
    {
        Vector2 rectSize = new Vector2( _transform.rect.width, _transform.rect.height);

        return rectSize;
    }

    /// <summary>
    /// Turns on/off fullscreen mode.
    /// </summary>
    public static void FullScreenMode( )
    {
        Screen.fullScreen = !Screen.fullScreen;
    }
    
    public static class GeoCodeCalc {
        public const double EarthRadiusInMiles = 3956.0;
        public const double EarthRadiusInKilometers = 6367.0;
        public const double EarthRadiusInMetres = 6371000;

        public static double ToRadian(double val) { return val * (Math.PI / 180); }
        public static double DiffRadian(double val1, double val2) { return ToRadian(val2) - ToRadian(val1); }

        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2){
            return CalcDistance(lat1, lng1, lat2, lng2, GeoCodeCalcMeasurement.Miles);
        }

        public static double CalcDistance(double lat1, double lng1, double lat2, double lng2, GeoCodeCalcMeasurement m){
            double radius = GeoCodeCalc.EarthRadiusInMiles;

            if (m == GeoCodeCalcMeasurement.Kilometers) { radius = GeoCodeCalc.EarthRadiusInKilometers; }
            if (m == GeoCodeCalcMeasurement.Metre) { radius = GeoCodeCalc.EarthRadiusInMetres; }
            return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
        }
    }

    public enum GeoCodeCalcMeasurement : int {
        Miles = 0,
        Kilometers = 1,
        Metre = 2
    }
    
}
